FROM node:14-alpine as base
ENV SENTRY_URL=$ARG_SENTRY_URL
RUN mkdir /app
WORKDIR /app
# RUN npm i yarn -g
RUN yarn global add @quasar/cli@1.0.3
ADD package.json app/package.json
ADD . /app
RUN yarn
LABEL enviroment="dev"
CMD ["quasar", "dev" , "-m", "pwa"]



FROM node:14-alpine as builder
RUN mkdir /app
WORKDIR /app
RUN yarn global add @quasar/cli@1.0.3
ADD package.json /app/package.json
ADD . /app
RUN yarn
LABEL enviroment="build"
RUN quasar build -m pwa

FROM nginx:alpine as server
COPY --from=builder /app/dist/pwa /usr/share/nginx/html/
COPY --from=builder /app/src/robots.txt /usr/share/nginx/html/robots.txt
ADD ./nginx.conf  /etc/nginx/conf.d/default.conf
LABEL enviroment="prod"
